import numpy, sys
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D

filename = sys.argv[1]

fig = plt.figure()
ax = Axes3D(fig)
fig.set_size_inches(6,6)

coords=[]
with open(filename, 'r') as f:
	for line in f.readlines():
		coords.append(line.split(','))


for ii in range(len(coords)):
	coord = coords[ii]
	plt.plot([numpy.float64(coord[1])], [numpy.float64(coord[2])], "ko", zs=[numpy.float64(coord[3])])
	if ii < len(coords) - 1:
		coord2 = coords[ii+1]
		plt.plot((numpy.float64(coord[1]), numpy.float64(coord2[1])), (numpy.float64(coord[2]), numpy.float64(coord2[2])), "k-", zs=(numpy.float64(coord[3]), numpy.float64(coord2[3])))

plt.show()
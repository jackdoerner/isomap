import numpy, sys
import matplotlib.pyplot as plt

filename = sys.argv[1]

fig, ax = plt.subplots()
fig.set_size_inches(6,6)

coords=[]
with open(filename, 'r') as f:
	for line in f.readlines():
		coords.append(line.split(','))


for ii in range(len(coords)):
	coord = coords[ii]
	plt.plot(numpy.float64(coord[1]), numpy.float64(coord[2]), "ko")
	if ii < len(coords) -1:
		coord2 = coords[ii + 1]
		plt.plot((numpy.float64(coord[1]), numpy.float64(coord2[1])), (numpy.float64(coord[2]), numpy.float64(coord2[2])), "k-")

plt.show()
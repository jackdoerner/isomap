import numpy, sys
import spherefitting

file_base = sys.argv[1]
geodesic_file = file_base + "_geodesic.csv"
embedded_file = file_base + "_embedded.csv"

if __name__ == "__main__":
	geodesic_dists = None
	embedded_coords = None
	images = []

	with open(geodesic_file, 'r') as f:
		lines = f.readlines()
		geodesic_dists = numpy.empty((len(lines), len(lines[0].split(','))-1))
		ii = 0
		for line in lines:
			splitline = line.split(',')
			geodesic_dists[ii] = numpy.asarray(splitline[1:])
			ii += 1

	with open(embedded_file, 'r') as f:
		lines = f.readlines()
		embedded_coords = numpy.empty((len(lines), 3))
		ii = 0
		for line in lines:
			splitline = line.split(',')
			images.append(splitline[0])
			embedded_coords[ii] = numpy.asarray(splitline[1:])
			ii += 1

	two_coords = spherefitting.unfold(embedded_coords, geodesic_dists)

	with open(file_base + '_unfolded.csv', 'w') as output:
		for ii in range(embedded_coords.shape[0]):
			output.write(images[ii] + "," + str(two_coords[ii, 0]) + "," + str(two_coords[ii, 1]) + "\n")
	
